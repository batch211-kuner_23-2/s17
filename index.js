console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	function printMessage() {
	let fullName = prompt("Enter your Full Name:");
	let userAge = prompt("Enter your age:");
	let userLocation = prompt("Enter your location:")
	console.log("Hello" + " " + fullName + "!");
	console.log("You are" + " " + userAge + " " + "years old.");
	console.log("You live in" + " " + userLocation + ".");
};
printMessage();

	function thankMessage() {
		alert("Thank you for your input!");
	};
	// for alert thanks message
	thankMessage();

	// for alert add name of friends
		function printFriends () {
	alert("Hi! Please add the names of your friends.");
	};
	printFriends();

	//for prompt add name of friends
	function myFriend () {
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
	const myFriend = function () {
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);
	}
	myFriend();
}
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/
	function myFavBands() {
		let band1 = "Jayson in Town";
		let band2 = "Ben & Ben";
		let band3 = "Hillsongs";
		let band4 = "Eraseheads";
		let band5 = "Metallica";
		console.log("1." + band1);
		console.log("2." + band2);
		console.log("3." + band3);
		console.log("4." + band4);
		console.log("5." + band5);
	};
	myFavBands();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	function myFavMovies() {
		let movie1 = "Harry Poter";
		let movie2 = "Spider Man";
		let movie3 = "Transformer";
		let movie4 = "Avatar";
		let movie5 = "NeedforSpeed";
		console.log("1." + movie1 + "\nRottens tomatoes rating: 96%");
		console.log("2." + movie2 + "\nRottens tomatoes rating: 85%");
		console.log("3." + movie3 + "\nRottens tomatoes rating: 90%");
		console.log("4." + movie4 + "\nRottens tomatoes rating: 89%");
		console.log("5." + movie5 + "\nRottens tomatoes rating: 93%");
	}
	myFavMovies();

	myFriend();
	
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	// function myFriend () {
	// let friend1 = prompt("Enter your first friend's name:"); 
	// let friend2 = prompt("Enter your second friend's name:"); 
	// let friend3 = prompt("Enter your third friend's name:");
	// console.log(friend1); 
	// console.log(friend2); 
	// console.log(friend3); 
	// }
	// myFriend();
	

//console.log(friend1);
//console.log(friend2);